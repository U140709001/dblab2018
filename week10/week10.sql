SELECT * FROM company.Customers;

explain  Customers;

create view myview as
select * from Customers
order by CustomerName;

create index abc2 on Customers(CustomerName);

explain Customers;

select *from myview;

insert into myview
values(10000,"a","a","a","a","a","a");

create view myview2 as
select Customers.*,Orders.CustomerID c
from Customers,Orders
where Customers.CustomerID = Orders.OrderID;

select * from myview2;

